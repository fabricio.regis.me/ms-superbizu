package br.com.superbizu;


public class PublicacaDTO {
	
	
	private String tipo;
	
	private String descricao;
	
	private Long idListaQuestoes;
	
	private Long idDocumento;
	
	private String docDescriccao;
	
	private String docLink;
	
	private String docImagemLink;
	
	private String tipoDocumento;
	
	private String textoDocumento;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getIdListaQuestoes() {
		return idListaQuestoes;
	}

	public void setIdListaQuestoes(Long idListaQuestoes) {
		this.idListaQuestoes = idListaQuestoes;
	}

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getDocDescriccao() {
		return docDescriccao;
	}

	public void setDocDescriccao(String docDescriccao) {
		this.docDescriccao = docDescriccao;
	}

	public String getDocLink() {
		return docLink;
	}

	public void setDocLink(String docLink) {
		this.docLink = docLink;
	}

	public String getDocImagemLink() {
		return docImagemLink;
	}

	public void setDocImagemLink(String docImagemLink) {
		this.docImagemLink = docImagemLink;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTextoDocumento() {
		return textoDocumento;
	}

	public void setTextoDocumento(String textoDocumento) {
		this.textoDocumento = textoDocumento;
	}
	
	
	
	
	/*
	 * 'LIST' else 'DOCUMENT' end as POST_TYPE, "
	+" d.DESCRIPTION AS DESCRI, p.IDLISTAQUESTOES AS IDLIST, " 
	+" p.IDDOCUMENT AS IDDOC,d.DESCRIPTION AS DOCUMENT_DESCRIPTION, d.DOCUMENT_LINK DOCLINK,d.IMAGE_LINK IMAGE_DOCUMENT_LINK, "
	+	" case when d.TYPE = 1 then 'MOVIE' "
	+	"  when d.TYPE = 2 then 'ARTICLE'  "
	+   " when d.TYPE = 3 then 'DOCUMENT'end as DOCUMENT_TYPE, "
	+" d.TEXT_DOCUMENT AS TEXTODOC,d.IDDOCUMENT AS IDOCUMENTO 
	 */

}
