/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu;

/**
 *
 * @author lucianosantos
 */
public class ItemDTO {
    
    
    private Long itemId;
    private String descriptionItem;
    private String itemLetter;
    private String nameImageItem;
    
    
    public String getDescriptionItem() {
        return descriptionItem;
    }

    public void setDescriptionItem(String descriptionItem) {
        this.descriptionItem = descriptionItem;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemLetter() {
        return itemLetter;
    }

    public void setItemLetter(String itemLetter) {
        this.itemLetter = itemLetter;
    }

    public String getNameImageItem() {
        return nameImageItem;
    }

    public void setNameImageItem(String nameImageItem) {
        this.nameImageItem = nameImageItem;
    }
    
}
