/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu;

import br.com.superbizu.domain.TbItem;
import java.util.List;

/**
 *
 * @author Luciano
 */
public class QuestionListDTO {
    
    private Long idQuestionList;
    private String listDescription;
    private List<QuestionDTO> questions;
    //private List<TbItem> itens; 

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }
    
     public Long getIdQuestionList() {
        return idQuestionList;
    }

    public void setIdQuestionList(Long idQuestionList) {
        this.idQuestionList = idQuestionList;
    }

    public String getListDescription() {
        return listDescription;
    }

    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }
    
}
