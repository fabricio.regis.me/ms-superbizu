package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the lista_questoes database table.
 * 
 */
@Entity
@Table(name="lista_questoes")
@NamedQuery(name="ListaQuestoe.findAll", query="SELECT l FROM ListaQuestoe l")
public class ListaQuestoe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String idlistaquestoes;

	private String descricao;

	private int idempresa;

	private int idresponsavel;

	private String verrespostaduranteresolucao;

	public ListaQuestoe() {
	}

	public String getIdlistaquestoes() {
		return this.idlistaquestoes;
	}

	public void setIdlistaquestoes(String idlistaquestoes) {
		this.idlistaquestoes = idlistaquestoes;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getIdempresa() {
		return this.idempresa;
	}

	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public int getIdresponsavel() {
		return this.idresponsavel;
	}

	public void setIdresponsavel(int idresponsavel) {
		this.idresponsavel = idresponsavel;
	}

	public String getVerrespostaduranteresolucao() {
		return this.verrespostaduranteresolucao;
	}

	public void setVerrespostaduranteresolucao(String verrespostaduranteresolucao) {
		this.verrespostaduranteresolucao = verrespostaduranteresolucao;
	}

}