package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the DOCUMENTS database table.
 * 
 */
@Entity
@Table(name="DOCUMENTS")
@NamedQuery(name="Document.findAll", query="SELECT d FROM Document d")
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int iddocument;

	private Timestamp date;

	private String description;

	@Column(name="DOCUMENT_LINK")
	private String documentLink;

	@Column(name="ID_TEACHER")
	private int idTeacher;

	@Column(name="IMAGE_LINK")
	private String imageLink;

	@Column(name="TEXT_DOCUMENT")
	private String textDocument;

	private String type;

	//bi-directional many-to-one association to GroupDocument
	@OneToMany(mappedBy="document")
	private List<GroupDocument> groupDocuments;

	public Document() {
	}

	public int getIddocument() {
		return this.iddocument;
	}

	public void setIddocument(int iddocument) {
		this.iddocument = iddocument;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDocumentLink() {
		return this.documentLink;
	}

	public void setDocumentLink(String documentLink) {
		this.documentLink = documentLink;
	}

	public int getIdTeacher() {
		return this.idTeacher;
	}

	public void setIdTeacher(int idTeacher) {
		this.idTeacher = idTeacher;
	}

	public String getImageLink() {
		return this.imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getTextDocument() {
		return this.textDocument;
	}

	public void setTextDocument(String textDocument) {
		this.textDocument = textDocument;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<GroupDocument> getGroupDocuments() {
		return this.groupDocuments;
	}

	public void setGroupDocuments(List<GroupDocument> groupDocuments) {
		this.groupDocuments = groupDocuments;
	}

	public GroupDocument addGroupDocument(GroupDocument groupDocument) {
		getGroupDocuments().add(groupDocument);
		groupDocument.setDocument(this);

		return groupDocument;
	}

	public GroupDocument removeGroupDocument(GroupDocument groupDocument) {
		getGroupDocuments().remove(groupDocument);
		groupDocument.setDocument(null);

		return groupDocument;
	}

}