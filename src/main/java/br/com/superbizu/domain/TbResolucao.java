package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the TB_RESOLUCAO database table.
 * 
 */
@Entity
@Table(name="TB_RESOLUCAO")
@NamedQuery(name="TbResolucao.findAll", query="SELECT t FROM TbResolucao t")
public class TbResolucao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idResolucao;

	private Timestamp date;

	@Column(name="guid_resolucao")
	private String guidResolucao;

	@Column(name="ID_GRUPO_QUESTAO")
	private String idGrupoQuestao;

	@Column(name="ID_ITEM")
	private int idItem;

	@Column(name="ID_LISTA")
	private int idLista;

	@Column(name="ID_QUESTAO")
	private int idQuestao;

	@Column(name="ID_SIMULADO")
	private String idSimulado;

	@Column(name="ID_USUARIO")
	private int idUsuario;

	public TbResolucao() {
	}

	public int getIdResolucao() {
		return this.idResolucao;
	}

	public void setIdResolucao(int idResolucao) {
		this.idResolucao = idResolucao;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getGuidResolucao() {
		return this.guidResolucao;
	}

	public void setGuidResolucao(String guidResolucao) {
		this.guidResolucao = guidResolucao;
	}

	public String getIdGrupoQuestao() {
		return this.idGrupoQuestao;
	}

	public void setIdGrupoQuestao(String idGrupoQuestao) {
		this.idGrupoQuestao = idGrupoQuestao;
	}

	public int getIdItem() {
		return this.idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public int getIdLista() {
		return this.idLista;
	}

	public void setIdLista(int idLista) {
		this.idLista = idLista;
	}

	public int getIdQuestao() {
		return this.idQuestao;
	}

	public void setIdQuestao(int idQuestao) {
		this.idQuestao = idQuestao;
	}

	public String getIdSimulado() {
		return this.idSimulado;
	}

	public void setIdSimulado(String idSimulado) {
		this.idSimulado = idSimulado;
	}

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

}