package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the pessoa database table.
 * 
 */
@Embeddable
public class PessoaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int idpessoa;

	private int idempresa;

	public PessoaPK() {
	}
	public int getIdpessoa() {
		return this.idpessoa;
	}
	public void setIdpessoa(int idpessoa) {
		this.idpessoa = idpessoa;
	}
	public int getIdempresa() {
		return this.idempresa;
	}
	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PessoaPK)) {
			return false;
		}
		PessoaPK castOther = (PessoaPK)other;
		return 
			(this.idpessoa == castOther.idpessoa)
			&& (this.idempresa == castOther.idempresa);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idpessoa;
		hash = hash * prime + this.idempresa;
		
		return hash;
	}
}