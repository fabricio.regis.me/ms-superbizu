package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_materia database table.
 * 
 */
@Entity
@Table(name="tb_materia")
@NamedQuery(name="TbMateria.findAll", query="SELECT t FROM TbMateria t")
public class TbMateria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MATERIA")
	private int idMateria;

	@Column(name="ID_DONO")
	private int idDono;

	@Column(name="NOME_MATERIA")
	private String nomeMateria;

	//bi-directional many-to-one association to TbAssunto
	@OneToMany(mappedBy="tbMateria")
	private List<TbAssunto> tbAssuntos;

	public TbMateria() {
	}

	public int getIdMateria() {
		return this.idMateria;
	}

	public void setIdMateria(int idMateria) {
		this.idMateria = idMateria;
	}

	public int getIdDono() {
		return this.idDono;
	}

	public void setIdDono(int idDono) {
		this.idDono = idDono;
	}

	public String getNomeMateria() {
		return this.nomeMateria;
	}

	public void setNomeMateria(String nomeMateria) {
		this.nomeMateria = nomeMateria;
	}

	public List<TbAssunto> getTbAssuntos() {
		return this.tbAssuntos;
	}

	public void setTbAssuntos(List<TbAssunto> tbAssuntos) {
		this.tbAssuntos = tbAssuntos;
	}

	public TbAssunto addTbAssunto(TbAssunto tbAssunto) {
		getTbAssuntos().add(tbAssunto);
		tbAssunto.setTbMateria(this);

		return tbAssunto;
	}

	public TbAssunto removeTbAssunto(TbAssunto tbAssunto) {
		getTbAssuntos().remove(tbAssunto);
		tbAssunto.setTbMateria(null);

		return tbAssunto;
	}

}