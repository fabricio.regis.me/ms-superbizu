package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_criacao_usuario")
	private Date dtCriacaoUsuario;

	private String email;

	private int idempresa;

	private String nome;

	@Column(name="nome_foto_sistema")
	private String nomeFotoSistema;

	@Column(name="nome_foto_usuario")
	private String nomeFotoUsuario;

	private String senha;

	//bi-directional many-to-one association to TbAlunoGrupo
	@OneToMany(mappedBy="usuario")
	private List<TbAlunoGrupo> tbAlunoGrupos;

	//bi-directional many-to-one association to TbGrupo
	@OneToMany(mappedBy="usuario")
	private List<TbGrupo> tbGrupos;

	public Usuario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDtCriacaoUsuario() {
		return this.dtCriacaoUsuario;
	}

	public void setDtCriacaoUsuario(Date dtCriacaoUsuario) {
		this.dtCriacaoUsuario = dtCriacaoUsuario;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdempresa() {
		return this.idempresa;
	}

	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFotoSistema() {
		return this.nomeFotoSistema;
	}

	public void setNomeFotoSistema(String nomeFotoSistema) {
		this.nomeFotoSistema = nomeFotoSistema;
	}

	public String getNomeFotoUsuario() {
		return this.nomeFotoUsuario;
	}

	public void setNomeFotoUsuario(String nomeFotoUsuario) {
		this.nomeFotoUsuario = nomeFotoUsuario;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<TbAlunoGrupo> getTbAlunoGrupos() {
		return this.tbAlunoGrupos;
	}

	public void setTbAlunoGrupos(List<TbAlunoGrupo> tbAlunoGrupos) {
		this.tbAlunoGrupos = tbAlunoGrupos;
	}

	public TbAlunoGrupo addTbAlunoGrupo(TbAlunoGrupo tbAlunoGrupo) {
		getTbAlunoGrupos().add(tbAlunoGrupo);
		tbAlunoGrupo.setUsuario(this);

		return tbAlunoGrupo;
	}

	public TbAlunoGrupo removeTbAlunoGrupo(TbAlunoGrupo tbAlunoGrupo) {
		getTbAlunoGrupos().remove(tbAlunoGrupo);
		tbAlunoGrupo.setUsuario(null);

		return tbAlunoGrupo;
	}

	public List<TbGrupo> getTbGrupos() {
		return this.tbGrupos;
	}

	public void setTbGrupos(List<TbGrupo> tbGrupos) {
		this.tbGrupos = tbGrupos;
	}

	public TbGrupo addTbGrupo(TbGrupo tbGrupo) {
		getTbGrupos().add(tbGrupo);
		tbGrupo.setUsuario(this);

		return tbGrupo;
	}

	public TbGrupo removeTbGrupo(TbGrupo tbGrupo) {
		getTbGrupos().remove(tbGrupo);
		tbGrupo.setUsuario(null);

		return tbGrupo;
	}

}