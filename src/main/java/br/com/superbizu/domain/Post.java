package br.com.superbizu.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the POSTS database table.
 * 
 */
@Entity
@Table(name="POSTS")
@NamedQuery(name="Post.findAll", query="SELECT p FROM Post p")
public class Post implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private int idposts;

	private Timestamp date;

	private int iddocument;

	private int idlistaquestoes;

	//bi-directional many-to-one association to UserReadPost
	@OneToMany(mappedBy="post")
	private List<UserReadPost> userReadPosts;

	public Post() {
	}

	public int getIdposts() {
		return this.idposts;
	}

	public void setIdposts(int idposts) {
		this.idposts = idposts;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getIddocument() {
		return this.iddocument;
	}

	public void setIddocument(int iddocument) {
		this.iddocument = iddocument;
	}

	public int getIdlistaquestoes() {
		return this.idlistaquestoes;
	}

	public void setIdlistaquestoes(int idlistaquestoes) {
		this.idlistaquestoes = idlistaquestoes;
	}

	public List<UserReadPost> getUserReadPosts() {
		return this.userReadPosts;
	}

	public void setUserReadPosts(List<UserReadPost> userReadPosts) {
		this.userReadPosts = userReadPosts;
	}

	public UserReadPost addUserReadPost(UserReadPost userReadPost) {
		getUserReadPosts().add(userReadPost);
		userReadPost.setPost(this);

		return userReadPost;
	}

	public UserReadPost removeUserReadPost(UserReadPost userReadPost) {
		getUserReadPosts().remove(userReadPost);
		userReadPost.setPost(null);

		return userReadPost;
	}

}