package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the questao_lista database table.
 * 
 */
@Embeddable
public class QuestaoListaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int idlistaquestao;

	private int idquestao;

	public QuestaoListaPK() {
	}
	public int getIdlistaquestao() {
		return this.idlistaquestao;
	}
	public void setIdlistaquestao(int idlistaquestao) {
		this.idlistaquestao = idlistaquestao;
	}
	public int getIdquestao() {
		return this.idquestao;
	}
	public void setIdquestao(int idquestao) {
		this.idquestao = idquestao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof QuestaoListaPK)) {
			return false;
		}
		QuestaoListaPK castOther = (QuestaoListaPK)other;
		return 
			(this.idlistaquestao == castOther.idlistaquestao)
			&& (this.idquestao == castOther.idquestao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idlistaquestao;
		hash = hash * prime + this.idquestao;
		
		return hash;
	}
}