package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the questao_lista database table.
 * 
 */
@Entity
@Table(name="questao_lista")
@NamedQuery(name="QuestaoLista.findAll", query="SELECT q FROM QuestaoLista q")
public class QuestaoLista implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private QuestaoListaPK id;

	public QuestaoLista() {
	}

	public QuestaoListaPK getId() {
		return this.id;
	}

	public void setId(QuestaoListaPK id) {
		this.id = id;
	}

}