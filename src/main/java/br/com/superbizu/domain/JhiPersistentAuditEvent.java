/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "jhi_persistent_audit_event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JhiPersistentAuditEvent.findAll", query = "SELECT j FROM JhiPersistentAuditEvent j")
    , @NamedQuery(name = "JhiPersistentAuditEvent.findByEventId", query = "SELECT j FROM JhiPersistentAuditEvent j WHERE j.eventId = :eventId")
    , @NamedQuery(name = "JhiPersistentAuditEvent.findByPrincipal", query = "SELECT j FROM JhiPersistentAuditEvent j WHERE j.principal = :principal")
    , @NamedQuery(name = "JhiPersistentAuditEvent.findByEventDate", query = "SELECT j FROM JhiPersistentAuditEvent j WHERE j.eventDate = :eventDate")
    , @NamedQuery(name = "JhiPersistentAuditEvent.findByEventType", query = "SELECT j FROM JhiPersistentAuditEvent j WHERE j.eventType = :eventType")})
public class JhiPersistentAuditEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "event_id")
    private Long eventId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "principal")
    private String principal;
    @Column(name = "event_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Size(max = 255)
    @Column(name = "event_type")
    private String eventType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "jhiPersistentAuditEvent")
    private Collection<JhiPersistentAuditEvtData> jhiPersistentAuditEvtDataCollection;

    public JhiPersistentAuditEvent() {
    }

    public JhiPersistentAuditEvent(Long eventId) {
        this.eventId = eventId;
    }

    public JhiPersistentAuditEvent(Long eventId, String principal) {
        this.eventId = eventId;
        this.principal = principal;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @XmlTransient
    public Collection<JhiPersistentAuditEvtData> getJhiPersistentAuditEvtDataCollection() {
        return jhiPersistentAuditEvtDataCollection;
    }

    public void setJhiPersistentAuditEvtDataCollection(Collection<JhiPersistentAuditEvtData> jhiPersistentAuditEvtDataCollection) {
        this.jhiPersistentAuditEvtDataCollection = jhiPersistentAuditEvtDataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventId != null ? eventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JhiPersistentAuditEvent)) {
            return false;
        }
        JhiPersistentAuditEvent other = (JhiPersistentAuditEvent) object;
        if ((this.eventId == null && other.eventId != null) || (this.eventId != null && !this.eventId.equals(other.eventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.JhiPersistentAuditEvent[ eventId=" + eventId + " ]";
    }
    
}
