/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "tb_competencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbCompetencias.findAll", query = "SELECT t FROM TbCompetencias t")
    , @NamedQuery(name = "TbCompetencias.findByIdCompetencia", query = "SELECT t FROM TbCompetencias t WHERE t.idCompetencia = :idCompetencia")
    , @NamedQuery(name = "TbCompetencias.findByNomeCompetencia", query = "SELECT t FROM TbCompetencias t WHERE t.nomeCompetencia = :nomeCompetencia")})
public class TbCompetencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COMPETENCIA")
    private Integer idCompetencia;
    @Size(max = 45)
    @Column(name = "NOME_COMPETENCIA")
    private String nomeCompetencia;

    public TbCompetencias() {
    }

    public TbCompetencias(Integer idCompetencia) {
        this.idCompetencia = idCompetencia;
    }

    public Integer getIdCompetencia() {
        return idCompetencia;
    }

    public void setIdCompetencia(Integer idCompetencia) {
        this.idCompetencia = idCompetencia;
    }

    public String getNomeCompetencia() {
        return nomeCompetencia;
    }

    public void setNomeCompetencia(String nomeCompetencia) {
        this.nomeCompetencia = nomeCompetencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompetencia != null ? idCompetencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbCompetencias)) {
            return false;
        }
        TbCompetencias other = (TbCompetencias) object;
        if ((this.idCompetencia == null && other.idCompetencia != null) || (this.idCompetencia != null && !this.idCompetencia.equals(other.idCompetencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.TbCompetencias[ idCompetencia=" + idCompetencia + " ]";
    }
    
}
