package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_questao_habilidades database table.
 * 
 */
@Entity
@Table(name="tb_questao_habilidades")
@NamedQuery(name="TbQuestaoHabilidade.findAll", query="SELECT t FROM TbQuestaoHabilidade t")
public class TbQuestaoHabilidade implements Serializable {
	private static final long serialVersionUID = 1L;
    
	@Id
	@Column(name="TB_HABILIDADES_ID_HABILIDADE")
	private int tbHabilidadesIdHabilidade;

	@Column(name="TB_QUESTAO_ID_QUESTAO")
	private int tbQuestaoIdQuestao;

	public TbQuestaoHabilidade() {
	}

	public int getTbHabilidadesIdHabilidade() {
		return this.tbHabilidadesIdHabilidade;
	}

	public void setTbHabilidadesIdHabilidade(int tbHabilidadesIdHabilidade) {
		this.tbHabilidadesIdHabilidade = tbHabilidadesIdHabilidade;
	}

	public int getTbQuestaoIdQuestao() {
		return this.tbQuestaoIdQuestao;
	}

	public void setTbQuestaoIdQuestao(int tbQuestaoIdQuestao) {
		this.tbQuestaoIdQuestao = tbQuestaoIdQuestao;
	}

}