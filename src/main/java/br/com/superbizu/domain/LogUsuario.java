package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the log_usuario database table.
 * 
 */
@Entity
@Table(name="log_usuario")
@NamedQuery(name="LogUsuario.findAll", query="SELECT l FROM LogUsuario l")
public class LogUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="comentario_questao")
	private String comentarioQuestao;

	private String descricao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_alteracao")
	private Date dtAlteracao;

	@Column(name="id_questao")
	private int idQuestao;

	@Column(name="id_usuario")
	private int idUsuario;

	public LogUsuario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComentarioQuestao() {
		return this.comentarioQuestao;
	}

	public void setComentarioQuestao(String comentarioQuestao) {
		this.comentarioQuestao = comentarioQuestao;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDtAlteracao() {
		return this.dtAlteracao;
	}

	public void setDtAlteracao(Date dtAlteracao) {
		this.dtAlteracao = dtAlteracao;
	}

	public int getIdQuestao() {
		return this.idQuestao;
	}

	public void setIdQuestao(int idQuestao) {
		this.idQuestao = idQuestao;
	}

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

}