package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the usuario_empresa database table.
 * 
 */
@Embeddable
public class UsuarioEmpresaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int idusuarioempresa;

	private int idusuario;

	private int idempresa;

	public UsuarioEmpresaPK() {
	}
	public int getIdusuarioempresa() {
		return this.idusuarioempresa;
	}
	public void setIdusuarioempresa(int idusuarioempresa) {
		this.idusuarioempresa = idusuarioempresa;
	}
	public int getIdusuario() {
		return this.idusuario;
	}
	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}
	public int getIdempresa() {
		return this.idempresa;
	}
	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UsuarioEmpresaPK)) {
			return false;
		}
		UsuarioEmpresaPK castOther = (UsuarioEmpresaPK)other;
		return 
			(this.idusuarioempresa == castOther.idusuarioempresa)
			&& (this.idusuario == castOther.idusuario)
			&& (this.idempresa == castOther.idempresa);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idusuarioempresa;
		hash = hash * prime + this.idusuario;
		hash = hash * prime + this.idempresa;
		
		return hash;
	}
}