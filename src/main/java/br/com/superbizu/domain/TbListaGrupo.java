package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_lista_grupo database table.
 * 
 */
@Entity
@Table(name="tb_lista_grupo")
@NamedQuery(name="TbListaGrupo.findAll", query="SELECT t FROM TbListaGrupo t")
public class TbListaGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idListaGrupo;

	private int idEmpresa;

	private int idLista;

	private int idUsuarioResponsavel;

	private int situacaoAcesso;

	//bi-directional many-to-one association to TbGrupo
	@ManyToOne
	@JoinColumn(name="idGrupo")
	private TbGrupo tbGrupo;

	public TbListaGrupo() {
	}

	public int getIdListaGrupo() {
		return this.idListaGrupo;
	}

	public void setIdListaGrupo(int idListaGrupo) {
		this.idListaGrupo = idListaGrupo;
	}

	public int getIdEmpresa() {
		return this.idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getIdLista() {
		return this.idLista;
	}

	public void setIdLista(int idLista) {
		this.idLista = idLista;
	}

	public int getIdUsuarioResponsavel() {
		return this.idUsuarioResponsavel;
	}

	public void setIdUsuarioResponsavel(int idUsuarioResponsavel) {
		this.idUsuarioResponsavel = idUsuarioResponsavel;
	}

	public int getSituacaoAcesso() {
		return this.situacaoAcesso;
	}

	public void setSituacaoAcesso(int situacaoAcesso) {
		this.situacaoAcesso = situacaoAcesso;
	}

	public TbGrupo getTbGrupo() {
		return this.tbGrupo;
	}

	public void setTbGrupo(TbGrupo tbGrupo) {
		this.tbGrupo = tbGrupo;
	}

}