package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_aluno_grupo database table.
 * 
 */
@Entity
@Table(name="tb_aluno_grupo")
@NamedQuery(name="TbAlunoGrupo.findAll", query="SELECT t FROM TbAlunoGrupo t")
public class TbAlunoGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idAlunoGrupo;

	private int idEmpresa;

	private int situacaoAlunoGrupo;

	//bi-directional many-to-one association to TbGrupo
	@ManyToOne
	@JoinColumn(name="idGrupo")
	private TbGrupo tbGrupo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;

	public TbAlunoGrupo() {
	}

	public int getIdAlunoGrupo() {
		return this.idAlunoGrupo;
	}

	public void setIdAlunoGrupo(int idAlunoGrupo) {
		this.idAlunoGrupo = idAlunoGrupo;
	}

	public int getIdEmpresa() {
		return this.idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public int getSituacaoAlunoGrupo() {
		return this.situacaoAlunoGrupo;
	}

	public void setSituacaoAlunoGrupo(int situacaoAlunoGrupo) {
		this.situacaoAlunoGrupo = situacaoAlunoGrupo;
	}

	public TbGrupo getTbGrupo() {
		return this.tbGrupo;
	}

	public void setTbGrupo(TbGrupo tbGrupo) {
		this.tbGrupo = tbGrupo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}