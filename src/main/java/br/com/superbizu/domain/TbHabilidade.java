package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_habilidades database table.
 * 
 */
@Entity
@Table(name="tb_habilidades")
@NamedQuery(name="TbHabilidade.findAll", query="SELECT t FROM TbHabilidade t")
public class TbHabilidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_HABILIDADE")
	private int idHabilidade;

	@Column(name="DESCRICAO_HABILIDADE")
	private String descricaoHabilidade;

	@Column(name="TB_COMPETENCIAS_ID_COMPETENCIA")
	private int tbCompetenciasIdCompetencia;

	public TbHabilidade() {
	}

	public int getIdHabilidade() {
		return this.idHabilidade;
	}

	public void setIdHabilidade(int idHabilidade) {
		this.idHabilidade = idHabilidade;
	}

	public String getDescricaoHabilidade() {
		return this.descricaoHabilidade;
	}

	public void setDescricaoHabilidade(String descricaoHabilidade) {
		this.descricaoHabilidade = descricaoHabilidade;
	}

	public int getTbCompetenciasIdCompetencia() {
		return this.tbCompetenciasIdCompetencia;
	}

	public void setTbCompetenciasIdCompetencia(int tbCompetenciasIdCompetencia) {
		this.tbCompetenciasIdCompetencia = tbCompetenciasIdCompetencia;
	}

}