package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_assunto_questao database table.
 * 
 */
@Entity
@Table(name="tb_assunto_questao")
@NamedQuery(name="TbAssuntoQuestao.findAll", query="SELECT t FROM TbAssuntoQuestao t")
public class TbAssuntoQuestao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_QUESTAO")
	private int idQuestao;

	@Column(name="ID_ASSUNTO")
	private int idAssunto;

	@Column(name="ID_DONO")
	private int idDono;

	public TbAssuntoQuestao() {
	}

	public int getIdQuestao() {
		return this.idQuestao;
	}

	public void setIdQuestao(int idQuestao) {
		this.idQuestao = idQuestao;
	}

	public int getIdAssunto() {
		return this.idAssunto;
	}

	public void setIdAssunto(int idAssunto) {
		this.idAssunto = idAssunto;
	}

	public int getIdDono() {
		return this.idDono;
	}

	public void setIdDono(int idDono) {
		this.idDono = idDono;
	}

}