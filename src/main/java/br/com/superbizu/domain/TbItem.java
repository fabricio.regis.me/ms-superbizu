package br.com.superbizu.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_item database table.
 * 
 */
@Entity
@Table(name="tb_item")
@NamedQuery(name="TbItem.findAll", query="SELECT t FROM TbItem t")
public class TbItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ITEM")
	private int itemId;

        @Column(name="descricao")
        private String descriptionItem;

        @Column(name="ID_DONO")
	private int ownerId;

        @Column(name="ID_QUESTAO")
	private int questionId;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getDescriptionItem() {
        return descriptionItem;
    }

    public void setDescriptionItem(String descriptionItem) {
        this.descriptionItem = descriptionItem;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getItemLetter() {
        return itemLetter;
    }

    public void setItemLetter(String itemLetter) {
        this.itemLetter = itemLetter;
    }

    public String getNameImageItem() {
        return nameImageItem;
    }

    public void setNameImageItem(String nameImageItem) {
        this.nameImageItem = nameImageItem;
    }

    public String getNameImageItemSystem() {
        return nameImageItemSystem;
    }

    public void setNameImageItemSystem(String nameImageItemSystem) {
        this.nameImageItemSystem = nameImageItemSystem;
    }

    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }

        @Column(name="LETRA_ITEM")
	private String itemLetter;

        @Column(name="NOME_IMAGEM_ITEM")
	private String nameImageItem;

	@JsonAlias(value = "nameImageItemSystem")
        @Column(name="NOME_IMAGEM_ITEM_SISTEMA")
	private String nameImageItemSystem;

	@JsonAlias(value = "situation")
         @Column(name="situacao")
        private int situation;

	public TbItem() {
	}

	

}