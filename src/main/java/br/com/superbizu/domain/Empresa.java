package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the empresa database table.
 * 
 */
@Entity
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idempresa;

	private String cnpj;

	private int idusuario;

	@Column(name="nome_empresa")
	private String nomeEmpresa;

	@Column(name="nome_foto_sistema")
	private String nomeFotoSistema;

	@Column(name="nome_foto_usuario")
	private String nomeFotoUsuario;

	private String sincronizacaoEntradaAplicativo;

	private String sincronizacaoFechamentoVenda;

	private String sincronizacaoRegistroProduto;

	private String telefone;

	private int tipoContaEmpresa;

	private int tipoEmpresa;

	public Empresa() {
	}

	public int getIdempresa() {
		return this.idempresa;
	}

	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public int getIdusuario() {
		return this.idusuario;
	}

	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}

	public String getNomeEmpresa() {
		return this.nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getNomeFotoSistema() {
		return this.nomeFotoSistema;
	}

	public void setNomeFotoSistema(String nomeFotoSistema) {
		this.nomeFotoSistema = nomeFotoSistema;
	}

	public String getNomeFotoUsuario() {
		return this.nomeFotoUsuario;
	}

	public void setNomeFotoUsuario(String nomeFotoUsuario) {
		this.nomeFotoUsuario = nomeFotoUsuario;
	}

	public String getSincronizacaoEntradaAplicativo() {
		return this.sincronizacaoEntradaAplicativo;
	}

	public void setSincronizacaoEntradaAplicativo(String sincronizacaoEntradaAplicativo) {
		this.sincronizacaoEntradaAplicativo = sincronizacaoEntradaAplicativo;
	}

	public String getSincronizacaoFechamentoVenda() {
		return this.sincronizacaoFechamentoVenda;
	}

	public void setSincronizacaoFechamentoVenda(String sincronizacaoFechamentoVenda) {
		this.sincronizacaoFechamentoVenda = sincronizacaoFechamentoVenda;
	}

	public String getSincronizacaoRegistroProduto() {
		return this.sincronizacaoRegistroProduto;
	}

	public void setSincronizacaoRegistroProduto(String sincronizacaoRegistroProduto) {
		this.sincronizacaoRegistroProduto = sincronizacaoRegistroProduto;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getTipoContaEmpresa() {
		return this.tipoContaEmpresa;
	}

	public void setTipoContaEmpresa(int tipoContaEmpresa) {
		this.tipoContaEmpresa = tipoContaEmpresa;
	}

	public int getTipoEmpresa() {
		return this.tipoEmpresa;
	}

	public void setTipoEmpresa(int tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

}