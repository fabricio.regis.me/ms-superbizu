
package br.com.superbizu.domain;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface PostRepository extends CrudRepository<Post, Long>{
	
	@Query(value = " SELECT P FROM Post P ", nativeQuery = false)
	List<Post> listarTodos();
        
	@Query(value =   
			" SELECT " 
	+" case "
	+"	when d.iddocument is null then 'LIST' else 'DOCUMENT' end as POST_TYPE, "
	+" d.DESCRIPTION AS DESCRI, p.IDLISTAQUESTOES AS IDLIST, " 
	+" p.IDDOCUMENT AS IDDOC,d.DESCRIPTION AS DOCUMENT_DESCRIPTION, d.DOCUMENT_LINK DOCLINK,d.IMAGE_LINK IMAGE_DOCUMENT_LINK, "
	+	" case when d.TYPE = 1 then 'MOVIE' "
	+	"  when d.TYPE = 2 then 'ARTICLE'  "
	+   " when d.TYPE = 3 then 'DOCUMENT'end as DOCUMENT_TYPE, "
	+" d.TEXT_DOCUMENT AS TEXTODOC,d.IDDOCUMENT AS IDOCUMENTO from POSTS p " 
	+" left join DOCUMENTS d on d.IDDOCUMENT = p.IDDOCUMENT "
	+" left join lista_questoes lq on lq.IDLISTAQUESTOES = p.IDLISTAQUESTOES "
			, nativeQuery = true)
	List<Object> listar();
        
        
        //TERCEIRO SERVICO DO CASE WHEN COM PARAMETRO IDLISTAQUESTOES
        @Query(value = " SELECT lq.DESCRICAO LIST_DESCRIPTION,ql.IDQUESTAO LIST_ID,"
                     + " q.id_questao QUESTION_ID, "+
                       " q.DESCRICAO_QUESTAO QUESTION_DESCRIPTION, "
                +      " q.COMANDO_QUESTAO QUESTION_COMMAND , " +
                       " q.nome_imagem_questao NAME_IMAGE_QUESTION, "
                +      " q.comentario_questao QUESTION_COMMENT,   " +
                       " q.letra_item_correto CORRECT_CHOICE " +
                        " FROM lista_questoes lq " +
                        " inner join tb_lista_grupo lg on lg.idLista = lq.idlistaquestoes " +
                        " inner join tb_grupo g on g.idGrupo = lg.idGrupo " +
                        " inner join tb_aluno_grupo ag on ag.idGrupo = g.idGrupo " +
                        " inner join usuario u on u.id = ag.idUsuario " +
                        " inner join questao_lista ql on ql.IDLISTAQUESTAO = lq.IDLISTAQUESTOES " +
                        " inner join tb_questao q on q.id_questao = ql.idquestao " +
                        " where ql.IDLISTAQUESTAO  = :listId ", nativeQuery = true)
	List<Object> listarByIdQuestList(@Param("listId") Long listId);
	
	@Query(value = " SELECT * FROM POSTS ", nativeQuery = true)
	List<Object> listarTudo();
        
        //SEGUNDO SERVICO COM PARAM emailUser e listId, acho que esse tá certo, pelo vc tinha dito que sim.
        @Query(value = " SELECT lq.DESCRICAO LIST_DESCRIPTION,ql.IDLISTAQUESTAO LIST_ID,"
                     + " q.id_questao QUESTION_ID, "+
                       " q.DESCRICAO_QUESTAO QUESTION_DESCRIPTION, "
                +      " q.COMANDO_QUESTAO QUESTION_COMMAND , " +
                       " q.nome_imagem_questao NAME_IMAGE_QUESTION, "
                +      " q.comentario_questao QUESTION_COMMENT,   " +
                       " q.letra_item_correto CORRECT_CHOICE " +
                        " FROM lista_questoes lq " +
                        " inner join tb_lista_grupo lg on lg.idLista = lq.idlistaquestoes " +
                        " inner join tb_grupo g on g.idGrupo = lg.idGrupo " +
                        " inner join tb_aluno_grupo ag on ag.idGrupo = g.idGrupo " +
                        " inner join usuario u on u.id = ag.idUsuario " +
                        " inner join questao_lista ql on ql.IDLISTAQUESTAO = lq.IDLISTAQUESTOES " +
                        " inner join tb_questao q on q.id_questao = ql.idquestao " +
                        " where u.email = :emailUser " +
                        " and ql.IDLISTAQUESTAO  = :listId ", nativeQuery = true)
        List<Object> listQuestionByEmail(
                @Param("emailUser") String emailUser,
                @Param("listId") Long listId);

}
