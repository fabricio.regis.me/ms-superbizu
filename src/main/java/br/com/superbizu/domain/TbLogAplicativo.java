package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_log_aplicativo database table.
 * 
 */
@Entity
@Table(name="tb_log_aplicativo")
@NamedQuery(name="TbLogAplicativo.findAll", query="SELECT t FROM TbLogAplicativo t")
public class TbLogAplicativo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idLog;

	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	private String email;

	private String tabela;

	private String tipoSincronizacao;

	public TbLogAplicativo() {
	}

	public int getIdLog() {
		return this.idLog;
	}

	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTabela() {
		return this.tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public String getTipoSincronizacao() {
		return this.tipoSincronizacao;
	}

	public void setTipoSincronizacao(String tipoSincronizacao) {
		this.tipoSincronizacao = tipoSincronizacao;
	}

}