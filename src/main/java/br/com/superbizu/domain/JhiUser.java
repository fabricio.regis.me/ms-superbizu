/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "jhi_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JhiUser.findAll", query = "SELECT j FROM JhiUser j")
    , @NamedQuery(name = "JhiUser.findById", query = "SELECT j FROM JhiUser j WHERE j.id = :id")
    , @NamedQuery(name = "JhiUser.findByLogin", query = "SELECT j FROM JhiUser j WHERE j.login = :login")
    , @NamedQuery(name = "JhiUser.findByPasswordHash", query = "SELECT j FROM JhiUser j WHERE j.passwordHash = :passwordHash")
    , @NamedQuery(name = "JhiUser.findByFirstName", query = "SELECT j FROM JhiUser j WHERE j.firstName = :firstName")
    , @NamedQuery(name = "JhiUser.findByLastName", query = "SELECT j FROM JhiUser j WHERE j.lastName = :lastName")
    , @NamedQuery(name = "JhiUser.findByEmail", query = "SELECT j FROM JhiUser j WHERE j.email = :email")
    , @NamedQuery(name = "JhiUser.findByImageUrl", query = "SELECT j FROM JhiUser j WHERE j.imageUrl = :imageUrl")
    , @NamedQuery(name = "JhiUser.findByActivated", query = "SELECT j FROM JhiUser j WHERE j.activated = :activated")
    , @NamedQuery(name = "JhiUser.findByLangKey", query = "SELECT j FROM JhiUser j WHERE j.langKey = :langKey")
    , @NamedQuery(name = "JhiUser.findByActivationKey", query = "SELECT j FROM JhiUser j WHERE j.activationKey = :activationKey")
    , @NamedQuery(name = "JhiUser.findByResetKey", query = "SELECT j FROM JhiUser j WHERE j.resetKey = :resetKey")
    , @NamedQuery(name = "JhiUser.findByCreatedBy", query = "SELECT j FROM JhiUser j WHERE j.createdBy = :createdBy")
    , @NamedQuery(name = "JhiUser.findByCreatedDate", query = "SELECT j FROM JhiUser j WHERE j.createdDate = :createdDate")
    , @NamedQuery(name = "JhiUser.findByResetDate", query = "SELECT j FROM JhiUser j WHERE j.resetDate = :resetDate")
    , @NamedQuery(name = "JhiUser.findByLastModifiedBy", query = "SELECT j FROM JhiUser j WHERE j.lastModifiedBy = :lastModifiedBy")
    , @NamedQuery(name = "JhiUser.findByLastModifiedDate", query = "SELECT j FROM JhiUser j WHERE j.lastModifiedDate = :lastModifiedDate")})
public class JhiUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "login")
    private String login;
    @Size(max = 60)
    @Column(name = "password_hash")
    private String passwordHash;
    @Size(max = 50)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 50)
    @Column(name = "last_name")
    private String lastName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 256)
    @Column(name = "image_url")
    private String imageUrl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activated")
    private boolean activated;
    @Size(max = 5)
    @Column(name = "lang_key")
    private String langKey;
    @Size(max = 20)
    @Column(name = "activation_key")
    private String activationKey;
    @Size(max = 20)
    @Column(name = "reset_key")
    private String resetKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "reset_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resetDate;
    @Size(max = 50)
    @Column(name = "last_modified_by")
    private String lastModifiedBy;
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @ManyToMany(mappedBy = "jhiUserCollection")
    private Collection<JhiAuthority> jhiAuthorityCollection;

    public JhiUser() {
    }

    public JhiUser(Long id) {
        this.id = id;
    }

    public JhiUser(Long id, String login, boolean activated, String createdBy, Date createdDate) {
        this.id = id;
        this.login = login;
        this.activated = activated;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getResetDate() {
        return resetDate;
    }

    public void setResetDate(Date resetDate) {
        this.resetDate = resetDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @XmlTransient
    public Collection<JhiAuthority> getJhiAuthorityCollection() {
        return jhiAuthorityCollection;
    }

    public void setJhiAuthorityCollection(Collection<JhiAuthority> jhiAuthorityCollection) {
        this.jhiAuthorityCollection = jhiAuthorityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JhiUser)) {
            return false;
        }
        JhiUser other = (JhiUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.JhiUser[ id=" + id + " ]";
    }
    
}
