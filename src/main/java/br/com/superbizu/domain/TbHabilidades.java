/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "tb_habilidades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbHabilidades.findAll", query = "SELECT t FROM TbHabilidades t")
    , @NamedQuery(name = "TbHabilidades.findByIdHabilidade", query = "SELECT t FROM TbHabilidades t WHERE t.idHabilidade = :idHabilidade")
    , @NamedQuery(name = "TbHabilidades.findByTbCompetenciasIdCompetencia", query = "SELECT t FROM TbHabilidades t WHERE t.tbCompetenciasIdCompetencia = :tbCompetenciasIdCompetencia")
    , @NamedQuery(name = "TbHabilidades.findByDescricaoHabilidade", query = "SELECT t FROM TbHabilidades t WHERE t.descricaoHabilidade = :descricaoHabilidade")})
public class TbHabilidades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_HABILIDADE")
    private Integer idHabilidade;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TB_COMPETENCIAS_ID_COMPETENCIA")
    private int tbCompetenciasIdCompetencia;
    @Size(max = 500)
    @Column(name = "DESCRICAO_HABILIDADE")
    private String descricaoHabilidade;

    public TbHabilidades() {
    }

    public TbHabilidades(Integer idHabilidade) {
        this.idHabilidade = idHabilidade;
    }

    public TbHabilidades(Integer idHabilidade, int tbCompetenciasIdCompetencia) {
        this.idHabilidade = idHabilidade;
        this.tbCompetenciasIdCompetencia = tbCompetenciasIdCompetencia;
    }

    public Integer getIdHabilidade() {
        return idHabilidade;
    }

    public void setIdHabilidade(Integer idHabilidade) {
        this.idHabilidade = idHabilidade;
    }

    public int getTbCompetenciasIdCompetencia() {
        return tbCompetenciasIdCompetencia;
    }

    public void setTbCompetenciasIdCompetencia(int tbCompetenciasIdCompetencia) {
        this.tbCompetenciasIdCompetencia = tbCompetenciasIdCompetencia;
    }

    public String getDescricaoHabilidade() {
        return descricaoHabilidade;
    }

    public void setDescricaoHabilidade(String descricaoHabilidade) {
        this.descricaoHabilidade = descricaoHabilidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHabilidade != null ? idHabilidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbHabilidades)) {
            return false;
        }
        TbHabilidades other = (TbHabilidades) object;
        if ((this.idHabilidade == null && other.idHabilidade != null) || (this.idHabilidade != null && !this.idHabilidade.equals(other.idHabilidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.TbHabilidades[ idHabilidade=" + idHabilidade + " ]";
    }
    
}
