package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the usuario_empresa database table.
 * 
 */
@Entity
@Table(name="usuario_empresa")
@NamedQuery(name="UsuarioEmpresa.findAll", query="SELECT u FROM UsuarioEmpresa u")
public class UsuarioEmpresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UsuarioEmpresaPK id;

	private String loginApp;

	private String loginSistema;

	@Column(name="usuario_master")
	private String usuarioMaster;

	public UsuarioEmpresa() {
	}

	public UsuarioEmpresaPK getId() {
		return this.id;
	}

	public void setId(UsuarioEmpresaPK id) {
		this.id = id;
	}

	public String getLoginApp() {
		return this.loginApp;
	}

	public void setLoginApp(String loginApp) {
		this.loginApp = loginApp;
	}

	public String getLoginSistema() {
		return this.loginSistema;
	}

	public void setLoginSistema(String loginSistema) {
		this.loginSistema = loginSistema;
	}

	public String getUsuarioMaster() {
		return this.usuarioMaster;
	}

	public void setUsuarioMaster(String usuarioMaster) {
		this.usuarioMaster = usuarioMaster;
	}

}