package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pessoa database table.
 * 
 */
@Entity
@NamedQuery(name="Pessoa.findAll", query="SELECT p FROM Pessoa p")
public class Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PessoaPK id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_alteracao_site")
	private Date dtAlteracaoSite;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_criacao_site")
	private Date dtCriacaoSite;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_primeira_sincronizacao")
	private Date dtPrimeiraSincronizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_ultima_sincronizacao")
	private Date dtUltimaSincronizacao;

	private String email;

	private String endereco;

	private String nome;

	private String site;

	private String telefone;

	public Pessoa() {
	}

	public PessoaPK getId() {
		return this.id;
	}

	public void setId(PessoaPK id) {
		this.id = id;
	}

	public Date getDtAlteracaoSite() {
		return this.dtAlteracaoSite;
	}

	public void setDtAlteracaoSite(Date dtAlteracaoSite) {
		this.dtAlteracaoSite = dtAlteracaoSite;
	}

	public Date getDtCriacaoSite() {
		return this.dtCriacaoSite;
	}

	public void setDtCriacaoSite(Date dtCriacaoSite) {
		this.dtCriacaoSite = dtCriacaoSite;
	}

	public Date getDtPrimeiraSincronizacao() {
		return this.dtPrimeiraSincronizacao;
	}

	public void setDtPrimeiraSincronizacao(Date dtPrimeiraSincronizacao) {
		this.dtPrimeiraSincronizacao = dtPrimeiraSincronizacao;
	}

	public Date getDtUltimaSincronizacao() {
		return this.dtUltimaSincronizacao;
	}

	public void setDtUltimaSincronizacao(Date dtUltimaSincronizacao) {
		this.dtUltimaSincronizacao = dtUltimaSincronizacao;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSite() {
		return this.site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}