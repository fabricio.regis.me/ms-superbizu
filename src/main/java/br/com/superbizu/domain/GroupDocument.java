package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the GROUP_DOCUMENTS database table.
 * 
 */
@Entity
@Table(name="GROUP_DOCUMENTS")
@NamedQuery(name="GroupDocument.findAll", query="SELECT g FROM GroupDocument g")
public class GroupDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idgroupdocuments;

	//bi-directional many-to-one association to TbGrupo
	@ManyToOne
	@JoinColumn(name="IDGROUP")
	private TbGrupo tbGrupo;

	//bi-directional many-to-one association to Document
	@ManyToOne
	@JoinColumn(name="IDDOCUMENT")
	private Document document;

	public GroupDocument() {
	}

	public int getIdgroupdocuments() {
		return this.idgroupdocuments;
	}

	public void setIdgroupdocuments(int idgroupdocuments) {
		this.idgroupdocuments = idgroupdocuments;
	}

	public TbGrupo getTbGrupo() {
		return this.tbGrupo;
	}

	public void setTbGrupo(TbGrupo tbGrupo) {
		this.tbGrupo = tbGrupo;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

}