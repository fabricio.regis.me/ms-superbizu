package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the USER_READ_POST database table.
 * 
 */
@Entity
@Table(name="USER_READ_POST")
@NamedQuery(name="UserReadPost.findAll", query="SELECT u FROM UserReadPost u")
public class UserReadPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int iduserreadpost;

	private Timestamp date;

	private int iduser;

	//bi-directional many-to-one association to Post
	@ManyToOne
	@JoinColumn(name="IDPOSTS")
	private Post post;

	public UserReadPost() {
	}

	public int getIduserreadpost() {
		return this.iduserreadpost;
	}

	public void setIduserreadpost(int iduserreadpost) {
		this.iduserreadpost = iduserreadpost;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getIduser() {
		return this.iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	public Post getPost() {
		return this.post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}