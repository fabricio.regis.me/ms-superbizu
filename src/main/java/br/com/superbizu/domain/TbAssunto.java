package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_assunto database table.
 * 
 */
@Entity
@Table(name="tb_assunto")
@NamedQuery(name="TbAssunto.findAll", query="SELECT t FROM TbAssunto t")
public class TbAssunto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ASSUNTO")
	private int idAssunto;

	@Column(name="DESCRICAO_ASSUNTO")
	private String descricaoAssunto;

	@Column(name="ID_ASSUNTO_PAI")
	private int idAssuntoPai;

	@Column(name="ID_DONO")
	private int idDono;

	//bi-directional many-to-one association to TbMateria
	@ManyToOne
	@JoinColumn(name="ID_MATERIA")
	private TbMateria tbMateria;

	public TbAssunto() {
	}

	public int getIdAssunto() {
		return this.idAssunto;
	}

	public void setIdAssunto(int idAssunto) {
		this.idAssunto = idAssunto;
	}

	public String getDescricaoAssunto() {
		return this.descricaoAssunto;
	}

	public void setDescricaoAssunto(String descricaoAssunto) {
		this.descricaoAssunto = descricaoAssunto;
	}

	public int getIdAssuntoPai() {
		return this.idAssuntoPai;
	}

	public void setIdAssuntoPai(int idAssuntoPai) {
		this.idAssuntoPai = idAssuntoPai;
	}

	public int getIdDono() {
		return this.idDono;
	}

	public void setIdDono(int idDono) {
		this.idDono = idDono;
	}

	public TbMateria getTbMateria() {
		return this.tbMateria;
	}

	public void setTbMateria(TbMateria tbMateria) {
		this.tbMateria = tbMateria;
	}

}