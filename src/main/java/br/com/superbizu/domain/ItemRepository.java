/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Luciano
 */
public interface ItemRepository extends CrudRepository<TbItem, Integer>{
    
    
    @Query(value = " Select i from TbItem i where "
                 + " i.questionId = :idQuestion ")
    List<TbItem> listByQuestion(@Param("idQuestion") Integer idQuestion);
    
    
    
       @Query(value = " SELECT ID_ITEM, ID_QUESTAO, DESCRICAO, " +
                      " SITUACAO, NOME_IMAGEM_ITEM, LETRA_ITEM, " +
                      " NOME_IMAGEM_ITEM_SISTEMA, " +
                      " ID_DONO FROM tb_item WHERE ID_QUESTAO = :idQuestion ", nativeQuery = true)
    List<Object> listByQuestionId(@Param("idQuestion") Integer idQuestion);
    
}

