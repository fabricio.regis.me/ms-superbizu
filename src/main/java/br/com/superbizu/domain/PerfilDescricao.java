package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the perfil_descricao database table.
 * 
 */
@Entity
@Table(name="perfil_descricao")
@NamedQuery(name="PerfilDescricao.findAll", query="SELECT p FROM PerfilDescricao p")
public class PerfilDescricao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idperfil;

	private String nomeperfil;

	public PerfilDescricao() {
	}

	public int getIdperfil() {
		return this.idperfil;
	}

	public void setIdperfil(int idperfil) {
		this.idperfil = idperfil;
	}

	public String getNomeperfil() {
		return this.nomeperfil;
	}

	public void setNomeperfil(String nomeperfil) {
		this.nomeperfil = nomeperfil;
	}

}