/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "jhi_authority")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JhiAuthority.findAll", query = "SELECT j FROM JhiAuthority j")
    , @NamedQuery(name = "JhiAuthority.findByName", query = "SELECT j FROM JhiAuthority j WHERE j.name = :name")})
public class JhiAuthority implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @JoinTable(name = "jhi_user_authority", joinColumns = {
        @JoinColumn(name = "authority_name", referencedColumnName = "name")}, inverseJoinColumns = {
        @JoinColumn(name = "user_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<JhiUser> jhiUserCollection;

    public JhiAuthority() {
    }

    public JhiAuthority(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<JhiUser> getJhiUserCollection() {
        return jhiUserCollection;
    }

    public void setJhiUserCollection(Collection<JhiUser> jhiUserCollection) {
        this.jhiUserCollection = jhiUserCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JhiAuthority)) {
            return false;
        }
        JhiAuthority other = (JhiAuthority) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.JhiAuthority[ name=" + name + " ]";
    }
    
}
