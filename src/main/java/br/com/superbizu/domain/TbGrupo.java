package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_grupo database table.
 * 
 */
@Entity
@Table(name="tb_grupo")
@NamedQuery(name="TbGrupo.findAll", query="SELECT t FROM TbGrupo t")
public class TbGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idGrupo;

	private int empresaGrupo;

	private String nomeGrupo;

	//bi-directional many-to-one association to GroupDocument
	@OneToMany(mappedBy="tbGrupo")
	private List<GroupDocument> groupDocuments;

	//bi-directional many-to-one association to TbAlunoGrupo
	@OneToMany(mappedBy="tbGrupo")
	private List<TbAlunoGrupo> tbAlunoGrupos;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="responsavelGrupo")
	private Usuario usuario;

	//bi-directional many-to-one association to TbListaGrupo
	@OneToMany(mappedBy="tbGrupo")
	private List<TbListaGrupo> tbListaGrupos;

	public TbGrupo() {
	}

	public int getIdGrupo() {
		return this.idGrupo;
	}

	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}

	public int getEmpresaGrupo() {
		return this.empresaGrupo;
	}

	public void setEmpresaGrupo(int empresaGrupo) {
		this.empresaGrupo = empresaGrupo;
	}

	public String getNomeGrupo() {
		return this.nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public List<GroupDocument> getGroupDocuments() {
		return this.groupDocuments;
	}

	public void setGroupDocuments(List<GroupDocument> groupDocuments) {
		this.groupDocuments = groupDocuments;
	}

	public GroupDocument addGroupDocument(GroupDocument groupDocument) {
		getGroupDocuments().add(groupDocument);
		groupDocument.setTbGrupo(this);

		return groupDocument;
	}

	public GroupDocument removeGroupDocument(GroupDocument groupDocument) {
		getGroupDocuments().remove(groupDocument);
		groupDocument.setTbGrupo(null);

		return groupDocument;
	}

	public List<TbAlunoGrupo> getTbAlunoGrupos() {
		return this.tbAlunoGrupos;
	}

	public void setTbAlunoGrupos(List<TbAlunoGrupo> tbAlunoGrupos) {
		this.tbAlunoGrupos = tbAlunoGrupos;
	}

	public TbAlunoGrupo addTbAlunoGrupo(TbAlunoGrupo tbAlunoGrupo) {
		getTbAlunoGrupos().add(tbAlunoGrupo);
		tbAlunoGrupo.setTbGrupo(this);

		return tbAlunoGrupo;
	}

	public TbAlunoGrupo removeTbAlunoGrupo(TbAlunoGrupo tbAlunoGrupo) {
		getTbAlunoGrupos().remove(tbAlunoGrupo);
		tbAlunoGrupo.setTbGrupo(null);

		return tbAlunoGrupo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<TbListaGrupo> getTbListaGrupos() {
		return this.tbListaGrupos;
	}

	public void setTbListaGrupos(List<TbListaGrupo> tbListaGrupos) {
		this.tbListaGrupos = tbListaGrupos;
	}

	public TbListaGrupo addTbListaGrupo(TbListaGrupo tbListaGrupo) {
		getTbListaGrupos().add(tbListaGrupo);
		tbListaGrupo.setTbGrupo(this);

		return tbListaGrupo;
	}

	public TbListaGrupo removeTbListaGrupo(TbListaGrupo tbListaGrupo) {
		getTbListaGrupos().remove(tbListaGrupo);
		tbListaGrupo.setTbGrupo(null);

		return tbListaGrupo;
	}

}