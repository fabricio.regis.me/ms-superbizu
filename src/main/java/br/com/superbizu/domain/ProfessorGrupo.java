package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the professor_grupo database table.
 * 
 */
@Entity
@Table(name="professor_grupo")
@NamedQuery(name="ProfessorGrupo.findAll", query="SELECT p FROM ProfessorGrupo p")
public class ProfessorGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProfessorGrupoPK id;

	public ProfessorGrupo() {
	}

	public ProfessorGrupoPK getId() {
		return this.id;
	}

	public void setId(ProfessorGrupoPK id) {
		this.id = id;
	}

}