package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the perfil_usuario database table.
 * 
 */
@Entity
@Table(name="perfil_usuario")
@NamedQuery(name="PerfilUsuario.findAll", query="SELECT p FROM PerfilUsuario p")
public class PerfilUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idperfil;

	private int idusuarioempresa;

	private int situacao;

	public PerfilUsuario() {
	}

	public int getIdperfil() {
		return this.idperfil;
	}

	public void setIdperfil(int idperfil) {
		this.idperfil = idperfil;
	}

	public int getIdusuarioempresa() {
		return this.idusuarioempresa;
	}

	public void setIdusuarioempresa(int idusuarioempresa) {
		this.idusuarioempresa = idusuarioempresa;
	}

	public int getSituacao() {
		return this.situacao;
	}

	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}

}