package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_questao database table.
 * 
 */
@Entity
@Table(name="tb_questao")
@NamedQuery(name="TbQuestao.findAll", query="SELECT t FROM TbQuestao t")
public class TbQuestao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_QUESTAO")
	private int idQuestao;

	@Column(name="ANO_QUESTAO")
	private int anoQuestao;

	private int aplicacao;

	@Column(name="COMANDO_QUESTAO")
	private String comandoQuestao;

	@Column(name="COMENTARIO_QUESTAO")
	private String comentarioQuestao;

	@Column(name="DESCRICAO_QUESTAO")
	private String descricaoQuestao;

	@Column(name="DIA_PROVA")
	private int diaProva;

	@Column(name="ID_DONO")
	private int idDono;

	@Column(name="LETRA_ITEM_CORRETO")
	private String letraItemCorreto;

	@Column(name="NOME_IMAGEM_QUESTAO")
	private String nomeImagemQuestao;

	@Column(name="NOME_IMAGEM_QUESTAO_SISTEMA")
	private String nomeImagemQuestaoSistema;

	@Column(name="NUMERO_QUESTAO")
	private int numeroQuestao;

	private int prova;

	private int situacao;

	public TbQuestao() {
	}

	public int getIdQuestao() {
		return this.idQuestao;
	}

	public void setIdQuestao(int idQuestao) {
		this.idQuestao = idQuestao;
	}

	public int getAnoQuestao() {
		return this.anoQuestao;
	}

	public void setAnoQuestao(int anoQuestao) {
		this.anoQuestao = anoQuestao;
	}

	public int getAplicacao() {
		return this.aplicacao;
	}

	public void setAplicacao(int aplicacao) {
		this.aplicacao = aplicacao;
	}

	public String getComandoQuestao() {
		return this.comandoQuestao;
	}

	public void setComandoQuestao(String comandoQuestao) {
		this.comandoQuestao = comandoQuestao;
	}

	public String getComentarioQuestao() {
		return this.comentarioQuestao;
	}

	public void setComentarioQuestao(String comentarioQuestao) {
		this.comentarioQuestao = comentarioQuestao;
	}

	public String getDescricaoQuestao() {
		return this.descricaoQuestao;
	}

	public void setDescricaoQuestao(String descricaoQuestao) {
		this.descricaoQuestao = descricaoQuestao;
	}

	public int getDiaProva() {
		return this.diaProva;
	}

	public void setDiaProva(int diaProva) {
		this.diaProva = diaProva;
	}

	public int getIdDono() {
		return this.idDono;
	}

	public void setIdDono(int idDono) {
		this.idDono = idDono;
	}

	public String getLetraItemCorreto() {
		return this.letraItemCorreto;
	}

	public void setLetraItemCorreto(String letraItemCorreto) {
		this.letraItemCorreto = letraItemCorreto;
	}

	public String getNomeImagemQuestao() {
		return this.nomeImagemQuestao;
	}

	public void setNomeImagemQuestao(String nomeImagemQuestao) {
		this.nomeImagemQuestao = nomeImagemQuestao;
	}

	public String getNomeImagemQuestaoSistema() {
		return this.nomeImagemQuestaoSistema;
	}

	public void setNomeImagemQuestaoSistema(String nomeImagemQuestaoSistema) {
		this.nomeImagemQuestaoSistema = nomeImagemQuestaoSistema;
	}

	public int getNumeroQuestao() {
		return this.numeroQuestao;
	}

	public void setNumeroQuestao(int numeroQuestao) {
		this.numeroQuestao = numeroQuestao;
	}

	public int getProva() {
		return this.prova;
	}

	public void setProva(int prova) {
		this.prova = prova;
	}

	public int getSituacao() {
		return this.situacao;
	}

	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}

}