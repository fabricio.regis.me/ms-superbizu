package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_competencias database table.
 * 
 */
@Entity
@Table(name="tb_competencias")
@NamedQuery(name="TbCompetencia.findAll", query="SELECT t FROM TbCompetencia t")
public class TbCompetencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_COMPETENCIA")
	private int idCompetencia;

	@Column(name="NOME_COMPETENCIA")
	private String nomeCompetencia;

	public TbCompetencia() {
	}

	public int getIdCompetencia() {
		return this.idCompetencia;
	}

	public void setIdCompetencia(int idCompetencia) {
		this.idCompetencia = idCompetencia;
	}

	public String getNomeCompetencia() {
		return this.nomeCompetencia;
	}

	public void setNomeCompetencia(String nomeCompetencia) {
		this.nomeCompetencia = nomeCompetencia;
	}

}