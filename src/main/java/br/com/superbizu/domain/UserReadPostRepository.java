package br.com.superbizu.domain;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserReadPostRepository extends CrudRepository<UserReadPost, Long>{
	
	@Query(value = " Select * from USER_READ_POST ", nativeQuery = true)
	List<Object> listarTodos();

}
