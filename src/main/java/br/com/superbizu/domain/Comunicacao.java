package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigInteger;


/**
 * The persistent class for the comunicacao database table.
 * 
 */
@Entity
@NamedQuery(name="Comunicacao.findAll", query="SELECT c FROM Comunicacao c")
public class Comunicacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="c_dcliente")
	private BigInteger cDcliente;

	@Column(name="d_ata")
	private Timestamp dAta;

	@Column(name="n_rmatriculagerente")
	private BigInteger nRmatriculagerente;

	@Column(name="s_qcomunicacao")
	private BigInteger sQcomunicacao;

	private byte situacao;

	@Column(name="t_exto")
	private String tExto;

	public Comunicacao() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigInteger getCDcliente() {
		return this.cDcliente;
	}

	public void setCDcliente(BigInteger cDcliente) {
		this.cDcliente = cDcliente;
	}

	public Timestamp getDAta() {
		return this.dAta;
	}

	public void setDAta(Timestamp dAta) {
		this.dAta = dAta;
	}

	public BigInteger getNRmatriculagerente() {
		return this.nRmatriculagerente;
	}

	public void setNRmatriculagerente(BigInteger nRmatriculagerente) {
		this.nRmatriculagerente = nRmatriculagerente;
	}

	public BigInteger getSQcomunicacao() {
		return this.sQcomunicacao;
	}

	public void setSQcomunicacao(BigInteger sQcomunicacao) {
		this.sQcomunicacao = sQcomunicacao;
	}

	public byte getSituacao() {
		return this.situacao;
	}

	public void setSituacao(byte situacao) {
		this.situacao = situacao;
	}

	public String getTExto() {
		return this.tExto;
	}

	public void setTExto(String tExto) {
		this.tExto = tExto;
	}

}