/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "jhi_persistent_audit_evt_data")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JhiPersistentAuditEvtData.findAll", query = "SELECT j FROM JhiPersistentAuditEvtData j")
    , @NamedQuery(name = "JhiPersistentAuditEvtData.findByEventId", query = "SELECT j FROM JhiPersistentAuditEvtData j WHERE j.jhiPersistentAuditEvtDataPK.eventId = :eventId")
    , @NamedQuery(name = "JhiPersistentAuditEvtData.findByName", query = "SELECT j FROM JhiPersistentAuditEvtData j WHERE j.jhiPersistentAuditEvtDataPK.name = :name")
    , @NamedQuery(name = "JhiPersistentAuditEvtData.findByValue", query = "SELECT j FROM JhiPersistentAuditEvtData j WHERE j.value = :value")})
public class JhiPersistentAuditEvtData implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected JhiPersistentAuditEvtDataPK jhiPersistentAuditEvtDataPK;
    @Size(max = 255)
    @Column(name = "value")
    private String value;
    @JoinColumn(name = "event_id", referencedColumnName = "event_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private JhiPersistentAuditEvent jhiPersistentAuditEvent;

    public JhiPersistentAuditEvtData() {
    }

    public JhiPersistentAuditEvtData(JhiPersistentAuditEvtDataPK jhiPersistentAuditEvtDataPK) {
        this.jhiPersistentAuditEvtDataPK = jhiPersistentAuditEvtDataPK;
    }

    public JhiPersistentAuditEvtData(long eventId, String name) {
        this.jhiPersistentAuditEvtDataPK = new JhiPersistentAuditEvtDataPK(eventId, name);
    }

    public JhiPersistentAuditEvtDataPK getJhiPersistentAuditEvtDataPK() {
        return jhiPersistentAuditEvtDataPK;
    }

    public void setJhiPersistentAuditEvtDataPK(JhiPersistentAuditEvtDataPK jhiPersistentAuditEvtDataPK) {
        this.jhiPersistentAuditEvtDataPK = jhiPersistentAuditEvtDataPK;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public JhiPersistentAuditEvent getJhiPersistentAuditEvent() {
        return jhiPersistentAuditEvent;
    }

    public void setJhiPersistentAuditEvent(JhiPersistentAuditEvent jhiPersistentAuditEvent) {
        this.jhiPersistentAuditEvent = jhiPersistentAuditEvent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jhiPersistentAuditEvtDataPK != null ? jhiPersistentAuditEvtDataPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JhiPersistentAuditEvtData)) {
            return false;
        }
        JhiPersistentAuditEvtData other = (JhiPersistentAuditEvtData) object;
        if ((this.jhiPersistentAuditEvtDataPK == null && other.jhiPersistentAuditEvtDataPK != null) || (this.jhiPersistentAuditEvtDataPK != null && !this.jhiPersistentAuditEvtDataPK.equals(other.jhiPersistentAuditEvtDataPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.JhiPersistentAuditEvtData[ jhiPersistentAuditEvtDataPK=" + jhiPersistentAuditEvtDataPK + " ]";
    }
    
}
