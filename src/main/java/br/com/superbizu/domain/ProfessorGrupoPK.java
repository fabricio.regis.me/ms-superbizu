package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the professor_grupo database table.
 * 
 */
@Embeddable
public class ProfessorGrupoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int idGrupo;

	private int idProfessor;

	private int situacao;

	public ProfessorGrupoPK() {
	}
	public int getIdGrupo() {
		return this.idGrupo;
	}
	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}
	public int getIdProfessor() {
		return this.idProfessor;
	}
	public void setIdProfessor(int idProfessor) {
		this.idProfessor = idProfessor;
	}
	public int getSituacao() {
		return this.situacao;
	}
	public void setSituacao(int situacao) {
		this.situacao = situacao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProfessorGrupoPK)) {
			return false;
		}
		ProfessorGrupoPK castOther = (ProfessorGrupoPK)other;
		return 
			(this.idGrupo == castOther.idGrupo)
			&& (this.idProfessor == castOther.idProfessor)
			&& (this.situacao == castOther.situacao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idGrupo;
		hash = hash * prime + this.idProfessor;
		hash = hash * prime + this.situacao;
		
		return hash;
	}
}