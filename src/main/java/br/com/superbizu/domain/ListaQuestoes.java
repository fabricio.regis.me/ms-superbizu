/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luciano
 */
@Entity
@Table(name = "lista_questoes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListaQuestoes.findAll", query = "SELECT l FROM ListaQuestoes l")
    , @NamedQuery(name = "ListaQuestoes.findByIdlistaquestoes", query = "SELECT l FROM ListaQuestoes l WHERE l.idlistaquestoes = :idlistaquestoes")
    , @NamedQuery(name = "ListaQuestoes.findByDescricao", query = "SELECT l FROM ListaQuestoes l WHERE l.descricao = :descricao")
    , @NamedQuery(name = "ListaQuestoes.findByIdempresa", query = "SELECT l FROM ListaQuestoes l WHERE l.idempresa = :idempresa")
    , @NamedQuery(name = "ListaQuestoes.findByIdresponsavel", query = "SELECT l FROM ListaQuestoes l WHERE l.idresponsavel = :idresponsavel")
    , @NamedQuery(name = "ListaQuestoes.findByVerrespostaduranteresolucao", query = "SELECT l FROM ListaQuestoes l WHERE l.verrespostaduranteresolucao = :verrespostaduranteresolucao")})
public class ListaQuestoes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDLISTAQUESTOES")
    private Long idlistaquestoes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDEMPRESA")
    private int idempresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDRESPONSAVEL")
    private int idresponsavel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VERRESPOSTADURANTERESOLUCAO")
    private Character verrespostaduranteresolucao;

    public ListaQuestoes() {
    }

    public ListaQuestoes(Long idlistaquestoes) {
        this.idlistaquestoes = idlistaquestoes;
    }

    public ListaQuestoes(Long idlistaquestoes, String descricao, int idempresa, int idresponsavel, Character verrespostaduranteresolucao) {
        this.idlistaquestoes = idlistaquestoes;
        this.descricao = descricao;
        this.idempresa = idempresa;
        this.idresponsavel = idresponsavel;
        this.verrespostaduranteresolucao = verrespostaduranteresolucao;
    }

    public Long getIdlistaquestoes() {
        return idlistaquestoes;
    }

    public void setIdlistaquestoes(Long idlistaquestoes) {
        this.idlistaquestoes = idlistaquestoes;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(int idempresa) {
        this.idempresa = idempresa;
    }

    public int getIdresponsavel() {
        return idresponsavel;
    }

    public void setIdresponsavel(int idresponsavel) {
        this.idresponsavel = idresponsavel;
    }

    public Character getVerrespostaduranteresolucao() {
        return verrespostaduranteresolucao;
    }

    public void setVerrespostaduranteresolucao(Character verrespostaduranteresolucao) {
        this.verrespostaduranteresolucao = verrespostaduranteresolucao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlistaquestoes != null ? idlistaquestoes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListaQuestoes)) {
            return false;
        }
        ListaQuestoes other = (ListaQuestoes) object;
        if ((this.idlistaquestoes == null && other.idlistaquestoes != null) || (this.idlistaquestoes != null && !this.idlistaquestoes.equals(other.idlistaquestoes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.superbizu.domain.ListaQuestoes[ idlistaquestoes=" + idlistaquestoes + " ]";
    }
    
}
