package br.com.superbizu.service;

import br.com.superbizu.ItemDTO;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.superbizu.PublicacaDTO;
import br.com.superbizu.QuestionDTO;
import br.com.superbizu.QuestionListDTO;
import br.com.superbizu.domain.ItemRepository;
import br.com.superbizu.domain.PostRepository;
import br.com.superbizu.domain.TbItem;
import br.com.superbizu.domain.UserReadPost;
import br.com.superbizu.domain.UserReadPostRepository;

@Service
public class BizuServiceImpl implements BizuService{
	
	@Autowired
	private PostRepository postRepository;
        
        @Autowired
        private ItemRepository itemRepository;
	
    @Autowired
	private UserReadPostRepository userReadPostRepository;
	

	@Override
	public List<PublicacaDTO> listar() {
		List<PublicacaDTO> lista = null;
		try {
			/*
			 * 'LIST' else 'DOCUMENT' end as POST_TYPE 0, "
	+" d.DESCRIPTION AS DESCRI 1, p.IDLISTAQUESTOES AS IDLIST 2, " 
	+" p.IDDOCUMENT AS IDDOC 3,d.DESCRIPTION AS DOCUMENT_DESCRIPTION 4,
	 d.DOCUMENT_LINK DOCLINK 5,d.IMAGE_LINK IMAGE_DOCUMENT_LINK 6, "
	+	" case when d.TYPE = 1 then 'MOVIE' "
	+	"  when d.TYPE = 2 then 'ARTICLE'  "
	+   " when d.TYPE = 3 then 'DOCUMENT'end as DOCUMENT_TYPE 7, "
	+" d.TEXT_DOCUMENT AS TEXTODOC 8,d.IDDOCUMENT AS IDOCUMENTO 9 
			 */
			List<Object> response = null;
			response = postRepository.listar();
			List<Object> objs = null;// postRepository.listarTudo();
			objs = postRepository.listar();
			if(objs != null && objs.size() > 0 ) {
				lista =  new ArrayList<PublicacaDTO>();
				for(Object o: objs) {
					Object[] ob = (Object[]) o;
					PublicacaDTO p = new PublicacaDTO();
					p.setTipo(ob[0] != null? String.valueOf(ob[0]):null);
					p.setDescricao(ob[1] != null? String.valueOf(ob[1]):null);
					p.setIdListaQuestoes(ob[2] != null? new Long(String.valueOf(ob[2])):null);
					p.setIdDocumento(ob[3] != null? new Long(String.valueOf(ob[3])):null);
					p.setDocDescriccao(ob[4] != null? String.valueOf(ob[4]):null);
					p.setDocLink(ob[5] != null? String.valueOf(ob[5]):null);
					p.setDocImagemLink(ob[6] != null? String.valueOf(ob[6]):null);
					p.setTipoDocumento(ob[7] != null? String.valueOf(ob[7]):null);
					p.setTextoDocumento(ob[8] != null? String.valueOf(ob[8]):null);
					p.setIdDocumento(ob[9] != null? new Long(String.valueOf(ob[9])):null);
					lista.add(p);
				}
			}
			
		} catch (Exception e) {
			new Exception("Erro ao executar listar()", e);
		}
		return lista;
	}


	@Override
	public List<UserReadPost> listarPostLidos() {
		List<UserReadPost> lista = null;
		try {
			List<Object> objs = userReadPostRepository.listarTodos();
			if(objs != null && objs.size() > 0 ) {
				lista = new ArrayList<UserReadPost>();
				for(Object o: objs) {
					
				}
			}
		} catch (Exception e) {
			new Exception("Erro ao executar listarPostLidos()", e);
		}
		return lista;
	}

    @Override
    public List<QuestionListDTO> listQuestionByListIdEmail(Long listId, String email) {
        List<QuestionListDTO> lista = null;
        try {
            List<Object> response = postRepository.listQuestionByEmail(email, listId);
            if(response != null) {
                lista = new ArrayList<QuestionListDTO>() ;
                List<QuestionDTO> listQuestDto = new ArrayList<QuestionDTO>();
                /*
                lq.DESCRICAO LIST_DESCRIPTION 0,ql.IDQUESTAO LIST_ID 1,q.id_questao QUESTION_ID 2, "+
                    " q.DESCRICAO_QUESTAO QUESTION_DESCRIPTION 3, q.COMANDO_QUESTAO QUESTION_COMMAND 4, " +
                    " q.nome_imagem_questao NAME_IMAGE_QUESTION 5,q.comentario_questao QUESTION_COMMENT 6, " +
                    " q.letra_item_correto CORRECT_CHOICE 7, i.DESCRICAO DESCRIPTION_ITEM 8, i.ID_ITEM ITEM_ID 9,"
                  + " i.LETRA_ITEM ITEM_LETTER 10, i.NOME_IMAGEM_ITEM_SISTEMA NAME_IMAGE_ITEM 11
                */
                for(Object ob: response) {
                    QuestionListDTO quest = new QuestionListDTO();
                    Object[] o = (Object[]) ob;
                    QuestionDTO q = new QuestionDTO();
                    quest.setListDescription(o[0] != null? String.valueOf(o[0]): null);
                    quest.setIdQuestionList(o[1] != null? new Long(String.valueOf(o[1])): null);
                    q.setIdQuestion(o[2] != null? new Long(String.valueOf(o[2])): null);
                    q.setQuestionDescripition(o[3] != null? String.valueOf(o[3]): null);
                    q.setQuestionCommand(o[4] != null? String.valueOf(o[4]): null);
                    q.setNameImageQuestion(o[5] != null? String.valueOf(o[5]): null);
                    q.setQuestionComment(o[6] != null? String.valueOf(o[6]): null);
                    q.setCorrectChoice(o[7] != null? String.valueOf(o[7]): null);
                    List<TbItem> itens = null;
                    List<Object> objs = itemRepository.listByQuestionId(new Integer(String.valueOf(q.getIdQuestion())));
                    if(objs != null && objs.size()> 0) {
                        itens =  new ArrayList<TbItem>();
                        for(Object obj: objs) {
                            Object[] i = (Object[]) obj;
                            TbItem it =  new TbItem();
                            it.setItemId(i[0] != null? new Integer(String.valueOf(i[0])): null );
                            it.setQuestionId(i[1] != null? new Integer(String.valueOf(i[1])): null );
                            it.setDescriptionItem(i[2] != null? String.valueOf(i[2]): null);
                            it.setSituation(i[3] != null? new Integer(String.valueOf(i[3])): null );
                            it.setNameImageItem(i[4] != null? String.valueOf(i[4]): null);
                            it.setItemLetter(i[5] != null? String.valueOf(i[5]): null);
                            it.setNameImageItem(i[6] != null? String.valueOf(i[6]): null);
                            itens.add(it);
                            
                        }
                    }
                    q.setItens(itens);
                    listQuestDto.add(q);
                    quest.setQuestions(listQuestDto);
                    //quest.setItens(itens);
                    lista.add(quest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            new Exception("Erro ao executar listQuestionByListIdEmail()", e);
        }
        return lista;
    }

    @Override
    public List<QuestionListDTO> listarByQuestListId(Long id) {
		  List<QuestionListDTO> lista = null;
        try {
            List<Object> response = postRepository.listarByIdQuestList(id);
            if(response != null) {
                lista = new ArrayList<QuestionListDTO>() ;
                List<QuestionDTO> listQuestDto = new ArrayList<QuestionDTO>();
                /*
                lq.DESCRICAO LIST_DESCRIPTION 0,ql.IDQUESTAO LIST_ID 1,q.id_questao QUESTION_ID 2, "+
                    " q.DESCRICAO_QUESTAO QUESTION_DESCRIPTION 3, q.COMANDO_QUESTAO QUESTION_COMMAND 4, " +
                    " q.nome_imagem_questao NAME_IMAGE_QUESTION 5,q.comentario_questao QUESTION_COMMENT 6, " +
                    " q.letra_item_correto CORRECT_CHOICE 7, i.DESCRICAO DESCRIPTION_ITEM 8, i.ID_ITEM ITEM_ID 9,"
                  + " i.LETRA_ITEM ITEM_LETTER 10, i.NOME_IMAGEM_ITEM_SISTEMA NAME_IMAGE_ITEM 11
                */
                for(Object ob: response) {
                    QuestionListDTO quest = new QuestionListDTO();
                    Object[] o = (Object[]) ob;
                    QuestionDTO q = new QuestionDTO();
                    quest.setListDescription(o[0] != null? String.valueOf(o[0]): null);
                    quest.setIdQuestionList(o[1] != null? new Long(String.valueOf(o[1])): null);
                    q.setIdQuestion(o[2] != null? new Long(String.valueOf(o[2])): null);
                    q.setQuestionDescripition(o[3] != null? String.valueOf(o[3]): null);
                    q.setQuestionCommand(o[4] != null? String.valueOf(o[4]): null);
                    q.setNameImageQuestion(o[5] != null? String.valueOf(o[5]): null);
                    q.setQuestionComment(o[6] != null? String.valueOf(o[6]): null);
                    q.setCorrectChoice(o[7] != null? String.valueOf(o[7]): null);
                    List<TbItem> itens = null;
                    List<Object> objs = itemRepository.listByQuestionId(new Integer(String.valueOf(q.getIdQuestion())));
                    if(objs != null && objs.size()> 0) {
                        itens =  new ArrayList<TbItem>();
                        for(Object obj: objs) {
                            Object[] i = (Object[]) obj;
                            TbItem it =  new TbItem();
                            it.setItemId(i[0] != null? new Integer(String.valueOf(i[0])): null );
                            it.setQuestionId(i[1] != null? new Integer(String.valueOf(i[1])): null );
                            it.setDescriptionItem(i[2] != null? String.valueOf(i[2]): null);
                            it.setSituation(i[3] != null? new Integer(String.valueOf(i[3])): null );
                            it.setNameImageItem(i[4] != null? String.valueOf(i[4]): null);
                            it.setItemLetter(i[5] != null? String.valueOf(i[5]): null);
                            it.setNameImageItem(i[6] != null? String.valueOf(i[6]): null);
                            itens.add(it);
                            
                        }
                    }
                    q.setItens(itens);
                    listQuestDto.add(q);
                    quest.setQuestions(listQuestDto);
                    //quest.setItens(itens);
                    lista.add(quest);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            new Exception("Erro ao executar listQuestionByListIdEmail()", e);
        }
        return lista;
    }
}



