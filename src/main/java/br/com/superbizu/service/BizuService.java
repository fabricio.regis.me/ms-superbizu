package br.com.superbizu.service;

import java.util.List;

import br.com.superbizu.PublicacaDTO;
import br.com.superbizu.QuestionListDTO;
import br.com.superbizu.domain.UserReadPost;

public interface BizuService {
	
	List<PublicacaDTO> listar(); 
	
	List<UserReadPost> listarPostLidos();
        
        List<QuestionListDTO> listQuestionByListIdEmail(Long listId, String email);
        
        List<QuestionListDTO> listarByQuestListId(Long id);
        

}
