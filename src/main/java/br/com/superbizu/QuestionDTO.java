/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.superbizu;

import br.com.superbizu.domain.TbItem;
import java.util.List;

/**
 *
 * @author lucianosantos
 */


public class QuestionDTO {
    
    private Long idQuestion;
    private String questionDescripition;
    private String questionCommand;
    private String nameImageQuestion;
    private String questionComment;
    private String correctChoice;
    
    private List<TbItem> itens;

    public List<TbItem> getItens() {
        return itens;
    }

    public void setItens(List<TbItem> itens) {
        this.itens = itens;
    }


    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestionDescripition() {
        return questionDescripition;
    }

    public void setQuestionDescripition(String questionDescripition) {
        this.questionDescripition = questionDescripition;
    }

    public String getQuestionCommand() {
        return questionCommand;
    }

    public void setQuestionCommand(String questionCommand) {
        this.questionCommand = questionCommand;
    }

    public String getNameImageQuestion() {
        return nameImageQuestion;
    }

    public void setNameImageQuestion(String nameImageQuestion) {
        this.nameImageQuestion = nameImageQuestion;
    }

    public String getQuestionComment() {
        return questionComment;
    }

    public void setQuestionComment(String questionComment) {
        this.questionComment = questionComment;
    }

    public String getCorrectChoice() {
        return correctChoice;
    }

    public void setCorrectChoice(String correctChoice) {
        this.correctChoice = correctChoice;
    }

    
}
