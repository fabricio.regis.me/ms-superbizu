package br.com.superbizu.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.superbizu.PublicacaDTO;
import br.com.superbizu.QuestionDTO;
import br.com.superbizu.QuestionListDTO;
import br.com.superbizu.domain.Post;
import br.com.superbizu.service.BizuService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class BizuController {

  
  @Autowired
  private BizuService bizuService;
  
  @RequestMapping(path = "/api/v1/documents/", method = RequestMethod.GET,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "List of all documents.", notes = "List of all documents ")
//  @ApiImplicitParams({
//      @ApiImplicitParam(name = "codigoPost", value = "Código do Post.",
//              required = true, dataType = "integer", paramType = "path")})
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Search done successfully.", response = Post.class)})
  public ResponseEntity<List<PublicacaDTO>> listAllPosts(
         // @PathVariable("codigoPost") Long codigoPost,
         // @RequestParam(value = "nome", required = false) String nome,
          )
  {
      List<PublicacaDTO> response = null;
      response = (List<PublicacaDTO>)bizuService.listar();
      
      return new ResponseEntity<List<PublicacaDTO>>(response,HttpStatus.OK);
  }
  
  
   @RequestMapping(path = "/api/v1/documents-idquestlist/", method = RequestMethod.GET,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "List question by idQuestList.", notes = "List question by idQuestList ")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "idQuestList", value = "Id Quest List",
              required = false, dataType = "long", paramType = "query")})
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Search done successfully.", response = Post.class)})
  public ResponseEntity<List<QuestionListDTO>> listPostsByIdQuestList(
         @RequestParam("idQuestList") Long id
         // @RequestParam(value = "nome", required = false) String nome,
          )
  {
      List<QuestionListDTO> response = null;
      response = (List<QuestionListDTO>)bizuService.listarByQuestListId(id);
      
      return new ResponseEntity<List<QuestionListDTO>>(response,HttpStatus.OK);
  }
  
  
    @RequestMapping(path = "/api/v1/questions/", method = RequestMethod.GET,
          produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "List question by email and listId.", notes = "List question by email and listId ")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "email", value = "email",
              required = false, dataType = "srting", paramType = "query"),
      @ApiImplicitParam(name = "listId", value = "listId",
              required = false, dataType = "long", paramType = "query")
  })
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Search done successfully.", response = QuestionDTO.class)})
  public ResponseEntity<List<QuestionListDTO>> listQuestionByEmailListId(
          @RequestParam(value = "email", required = false) String email,
          @RequestParam(value = "listId", required = false) Long listId
          )
  {
      List<QuestionListDTO> response = null;
      response = bizuService.listQuestionByListIdEmail(listId, email);
      if(response != null)
         return new ResponseEntity<List<QuestionListDTO>>(response,HttpStatus.OK);
      return new ResponseEntity<List<QuestionListDTO>>(HttpStatus.NOT_FOUND);
  }

  
}
